package az.ingress.ms14;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@EnableCaching
public class Ms14Application implements CommandLineRunner {


    public static void main(String[] args)
    {
        SpringApplication.run(Ms14Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
