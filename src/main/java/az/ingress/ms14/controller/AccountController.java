package az.ingress.ms14.controller;

import az.ingress.ms14.dto.AccountRequestDto;
import az.ingress.ms14.dto.AccountResponseDto;
import az.ingress.ms14.model.Account;
import az.ingress.ms14.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/{accountId}")
    public Account getById(@PathVariable Long accountId) {
        return accountService.getById(accountId);
    }

    @GetMapping("/all")
    public List<Account> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @PostMapping("/create")
    public AccountResponseDto create(@RequestBody AccountRequestDto request) {
        return accountService.create(request);
    }

    @PutMapping("/update/{accountId}")
    public Account update(@PathVariable Long accountId, @RequestBody Account account) {
        return accountService.update(accountId, account);
    }

    @DeleteMapping("/delete/{accountId}")
    public void deleteById(@PathVariable Long accountId) {
        accountService.deleteById(accountId);
    }

    @DeleteMapping("/delete/all")
    public void deleteAllAccounts(){
        accountService.deleteAllAccounts();
    }
}
