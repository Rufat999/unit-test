package az.ingress.ms14.service;

import org.springframework.stereotype.Service;

@Service
public class Calculator {

    public int sum(int a, int b) {
        return a + b;
    }

    public int divide(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("Divide to zero is prohibited");
        }
        return a / b;
    }
}
