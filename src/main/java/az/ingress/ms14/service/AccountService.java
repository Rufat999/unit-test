package az.ingress.ms14.service;

import az.ingress.ms14.dto.AccountRequestDto;
import az.ingress.ms14.dto.AccountResponseDto;
import az.ingress.ms14.model.Account;
import az.ingress.ms14.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final ModelMapper modelMapper;


    public Account getById(Long accountId) {
        Account account = accountRepository.findById(accountId)
                .orElseThrow(() -> new RuntimeException("Account " + accountId + " not found!"));
//        AccountResponseDto accountResponseDto = modelMapper.map(account, AccountResponseDto.class);
        return account;
    }

    public List<Account> getAllAccounts() {
        List<Account> accountList = accountRepository.findAll();
//        List<AccountResponseDto> accountResponseDtoList = accountList.stream().map(account -> modelMapper.map(account, AccountResponseDto.class)).collect(Collectors.toList());
        return accountList;
    }

    public AccountResponseDto create(AccountRequestDto request) {
        Account account = modelMapper.map(request, Account.class);
        accountRepository.save(account);
        AccountResponseDto accountResponseDto = modelMapper.map(account, AccountResponseDto.class);
        return accountResponseDto;
    }

    public Account update(Long accountId, Account account) {
        Account account1 = accountRepository.findById(accountId)
                .orElseThrow(() -> new RuntimeException("Account " + accountId + " not found!"));
        account1.setName(account.getName());
        account1.setBalance(account.getBalance());
        accountRepository.save(account1);
//        AccountResponseDto accountResponseDto = modelMapper.map(account, AccountResponseDto.class);
        return account1;
    }

    public void deleteById(Long accountId) {
        Account account = accountRepository.findById(accountId)
                .orElseThrow(() -> new RuntimeException("Account " + accountId + " not found!"));
        accountRepository.deleteById(accountId);
    }

    public void deleteAllAccounts() {
        accountRepository.deleteAll();
    }
}
