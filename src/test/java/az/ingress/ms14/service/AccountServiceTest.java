package az.ingress.ms14.service;


import az.ingress.ms14.dto.AccountRequestDto;
import az.ingress.ms14.dto.AccountResponseDto;
import az.ingress.ms14.model.Account;
import az.ingress.ms14.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private AccountRepository accountRepository;

    @Test
    void givenValidIdWhenGetAccountThenSuccess() {
        Account mockAccount = new Account();
        mockAccount.setId(1L);
        mockAccount.setName("Rufat");
        mockAccount.setBalance(100.0);

        when(accountRepository.findById(1L)).thenReturn(Optional.of(mockAccount));

        Account account = accountService.getById(1L);

        assertNotNull(account);
        assertThat(account.getId()).isEqualTo(1L);
        assertThat(account.getName()).isEqualTo("Rufat");
        assertThat(account.getBalance()).isEqualTo(100.0);
    }

    @Test
    void getAllAccountsThenSuccess() {
        Account account = new Account();
        account.setId(1L);

        Account account1 = new Account();
        account1.setId(2L);

        List<Account> mockAccounts = Arrays.asList(account, account1);

        when(accountRepository.findAll()).thenReturn(mockAccounts);

        List<Account> myAccounts = accountRepository.findAll();

        assertThat(mockAccounts.size()).isEqualTo(myAccounts.size());
        assertThat(mockAccounts.get(0)).isEqualTo(myAccounts.get(0));
        assertThat(mockAccounts.get(1)).isEqualTo(myAccounts.get(1));

        verify(accountRepository).findAll();
    }

    @Test
    void givenValidEntityUpdateEntity() {

        Account account = new Account();
        account.setId(1L);
        account.setName("Riyad");
        account.setBalance(100.0);

        Account newAccount = new Account();
        newAccount.setId(1L);
        newAccount.setName("Rufat");
        newAccount.setBalance(100.0);

        when(accountRepository.findById(1L)).thenReturn(Optional.of(account));
        when(accountRepository.save(any(Account.class))).thenReturn(newAccount);

        Account updatedAccount = accountService.update(1L, newAccount);

        verify(accountRepository, times(1)).findById(1L);
        verify(accountRepository, times(1)).save(newAccount);

        assertThat(newAccount).isEqualTo(updatedAccount);

    }

    @Test
    void testDeleteById() {
        Long id = 1L;

        accountRepository.deleteById(id);

        verify(accountRepository, times(1)).deleteById(id);
    }

    @Test
    void testDeleteAllAccounts(){

        accountService.deleteAllAccounts();

        verify(accountRepository, times(1)).deleteAll();
    }
}